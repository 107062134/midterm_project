# Software Studio 2020 Spring Midterm Project

## Topic
* Project Name : Chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|


# 作品網址：https://chat-room-6fef5.web.app/signin.html

# Components Description : 
### 1.登入頁面
 ![登入頁面](https://i.imgur.com/zm6wX1J.png =300x)
 : 這邊就申請帳號，然後作登入的動作，同時也可以用gmail.com做pop視窗的登入。

### 2.聊天室
![](https://i.imgur.com/BZ9V32j.png =200x)![](https://i.imgur.com/VhDYhOD.png =180x)
網頁上面的地方有聊天室(List)可以選擇，可以點擊你想要聊天的人的email，然後就會進到那個聊天室裡面，而List裡面的是所有曾經有註冊過的人這樣。
    public room就是大眾的聊天室可以當留言板這樣，而一開始登入進來，都會進到這個公共聊天室。



### 3.登出
![](https://i.imgur.com/fp6lNce.png =200x)
 就登出的地方

### 4.送訊息的按鈕
![](https://i.imgur.com/0qLlcnT.png =200x)
把訊息送出的按鈕

## Website Detail Description

#### 做自己的留言在右邊，別人的在左邊
![](https://i.imgur.com/Yk01WEq.png)

#### 分開私人跟公共的聊天室
![](https://i.imgur.com/p3g0DxM.png =200x)

#### 聊天室的數量要隨著註冊的人變多
![](https://i.imgur.com/g7k5ETc.png )

#### gmail登入的也要建立在我的名單裡面
![](https://i.imgur.com/X3aEcLk.png)


