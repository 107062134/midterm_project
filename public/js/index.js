
var user_email = '';
var another_user_email='';
var is_in_public=1;

function init() {
    var tt='test';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function() {
                firebase.auth().signOut()
                    .then(function() {
                        alert('Sign Out!')
                    })
                    .catch(function(error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    /*firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('list-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML="<span class='dropdown-item'> public room </span>";
            for(i=0;i<3;i++){
                menu.innerHTML = menu.innerHTML+"<span class='dropdown-item'>" + user.email + "</span>";
            }
        } else {
            menu.innerHTML = "";
            document.getElementById('post_list').innerHTML = "";
        }
    });*/
    var accountsRef = firebase.database().ref('account_list');
    var total_account = [];
    var first_count_account = 0;
    var second_count_account = 0;
    //console.log(is_in_public);
    accountsRef.on('child_added', function(data) {
        second_count_account += 1;
        if (second_count_account > first_count_account) {
            //var childData = data.val();
            var menu = document.getElementById('list-menu');
            if(user_email!=data.val().email){
                total_account[total_account.length] = "<button class='dropdown-item' value="+data.val().email+" id="+data.val().email +" onclick='another_user_email=value;is_in_public=0;init();'>" + data.val().email + "</button>";
                menu.innerHTML = total_account.join('');
            }
            first_count_account = total_account.length;
        }
    });
    public_btn=document.getElementById('public_room');
    public_btn.addEventListener('click',function(){
        is_in_public=1;
        update_message();
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            if(is_in_public){
                var newpostref = firebase.database().ref('com_list/').push();
                newpostref.set({
                    email: user_email,
                    data: post_txt.value
                });
                post_txt.value = "";
            }
            else{
                var newpostref = firebase.database().ref('private_list/'+user_email.substr(0,6)+'/'+another_user_email.substr(0,6)).push();
                newpostref.set({
                    email: user_email,
                    data: post_txt.value
                });
                var newpostref_again = firebase.database().ref('private_list/'+another_user_email.substr(0,6)+'/'+user_email.substr(0,6)).push();
                newpostref_again.set({
                    email: user_email,
                    data: post_txt.value
                });
                post_txt.value = "";
            }
        }
    });

    var str_before_username_my = "<div class ='w-100 row justify-content-end'><div class='w-50 my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted '><img src='https://i0.wp.com/ae01.alicdn.com/kf/HTB1OT2BPbPpK1RjSZFFq6y5PpXa8/1-Pcs-Auto-Car-Cartoon-JDM-Funny-Middle-Finger-Reflective-Vinyl-Car-Sticker-Motorcycle-Decal-Styling.jpg' alt='' class='mr-2 rounded ' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small  border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_before_username_other = "<div class ='w-100'><div class='w-50 my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted '><img src='https://i0.wp.com/ae01.alicdn.com/kf/HTB1OT2BPbPpK1RjSZFFq6y5PpXa8/1-Pcs-Auto-Car-Cartoon-JDM-Funny-Middle-Finger-Reflective-Vinyl-Car-Sticker-Motorcycle-Decal-Styling.jpg' alt='' class='mr-2 rounded ' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small  border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div></div>\n";
    console.log("A");
    update_message();
    function update_message(){
        document.getElementById('post_list').innerHTML='';
        console.log('AA');
        if(is_in_public){
            console.log("uu");
            var postsRef = firebase.database().ref('com_list');
            var total_post = [];
            var first_count = 0;
            var second_count = 0;

            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.email!=user_email){
                        total_post[total_post.length] = str_before_username_other + childData.email + "</strong>" + childData.data + str_after_content;
                    }
                    else{
                        total_post[total_post.length] = str_before_username_my + childData.email + "</strong>" + childData.data + str_after_content;
                    }
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    var scroll_bottom = document.getElementById('chat_room_space');
                    scroll_bottom.scrollTop=scroll_bottom.scrollHeight;
                    first_count = total_post.length;
                }
            });
        }
        else{
            console.log("00000");
            var postsRef = firebase.database().ref('private_list/'+user_email.substr(0,6)+'/'+another_user_email.substr(0,6));
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            console.log("here!");
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.email!=user_email){
                        total_post[total_post.length] = str_before_username_other + childData.email + "</strong>" + childData.data + str_after_content;
                    }
                    else{
                        total_post[total_post.length] = str_before_username_my + childData.email + "</strong>" + childData.data + str_after_content;
                    }
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    var scroll_bottom = document.getElementById('chat_room_space');
                    scroll_bottom.scrollTop=scroll_bottom.scrollHeight;
                    first_count = total_post.length;
                }
            });
        }
    }

    //$('chat_room_space').scrollTop(9999999);
}

window.onload = function() {
    init();
}